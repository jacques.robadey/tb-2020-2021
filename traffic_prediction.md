---
version: 2
titre: application de prévision de trafic et de temps de parcours
type de projet: Projet de bachelor
année scolaire: 2020/2021
filières:
  - Informatique
  - Télécommunications
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Jean Hennebert
mandants: [ville de Fribourg]
mots-clé: [smart city, google apps, web app, machine learning, statistics, internet des objets]
langue: [F, D]
confidentialité: non
suite: non
---
```{=tex}
\begin{center}
\includegraphics[width=0.7\textwidth]{img/circulation-voitures-prediction.jpg}
\end{center}
```

## Contexte

Si les technologies smart cities sont en phase de déploiement intensif avec des réseaux de capteurs distribués et de multiples sources d’information, les smart services tardent à venir modifier le quotidien des citadins. Ce projet a l’intension de créer des nouveaux services basés sur les informations récoltées par dans les smart cities.

Comme les villes de Fribourg et Bulle se dotent d’une infrastructure « smart city » et qu’un réseau de capteurs et de caméras de comptage de véhicules a été déployé (projet FRI-IoTnet), l’occasion est rêvée pour créer de nouveaux services intelligents. L’idée est d’aller au-delà du simple comptage et de prédire le trafic des prochaines 24 heures. Si un projet de semestre a déjà prouvé la faisabilité d’un tel service en se basant sur des données historiques, ce projet de bachelor a l’intention de le faire avec des données récentes prévoyant le trafic des prochaines 24 heures.
Un algorithme sera aussi développé pour calculer le temps de parcours entre les points principaux d’entrée et de sortie des villes à partir des mesures et des prévisions de trafic. Une web app pourra être développée sur cette base afin de conseiller l’utilisateur sur l’heure de départ idéale pour effectuer son trajet.

## Objectifs

Les objectifs et étapes du projet sont décrits ci-dessous de manière chronologique :

1)  Faire l'état de l'art des solutions utilisées pour les prévisions de trafic en incluant le travail réalisé à l’HEIA-FR
2)  Faire une recherche des applications déjà existantes prévoyant le temps de parcours des véhicules et rechercher la source d’information utilisée pour faire ces prévisions
3)  Utiliser les mesures de trafic effectuées par l’HEIA-FR et la ville de Fribourg pour prédire le trafic futur en se basant sur les travaux déjà effectués à l’HEIA-FR et à l’extérieur
4)  définir une stratégie pour le calcul du temps de parcours en fonction du trafic mesuré en se basant sur les applications du marché et sur des applications de calcul basiques utilisant la distance et la vitesse moyenne
5)  réaliser l’application web et la valider 

## Contraintes

Utiliser les mesures de la ville et du projet FRI-IoTnet, concentrer le travail sur les villes de Fribourg et Bulle 



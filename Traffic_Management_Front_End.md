---
version: 2
titre: le front-end d'un outil de gestion d'une ville intelligente
type de projet: Projet de bachelor
année scolaire: 2020/2021
filières:
  - Informatique
  - Télécommunications
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Serge Ayer
mandants: [ville de Fribourg]
mots-clé: [front-end, IoT, visualisation géographique et historique, interactive web app, stockage et formatage de données]
langue: [F, D]
confidentialité: non
suite: non
---
```{=tex}
\begin{center}
\includegraphics[width=1.4\textwidth]{img/Traffic_control.png}
\end{center}
```

## Contexte

Par le projet FRI-IoTnet, les villes de Fribourg et Bulle se sont dotées d'un réseau de télécommunication à bas débit et de caméras de comptage de véhicules, de sonomètres et de capteur de pollution. Le système doit permettre aux villes de connaître en tout temps leur densité de trafic sur les axes principaux ainsi que ses effets négatifs. Un élément clef du système est la plateforme de visualisation, qui doit montrer de manière interactive tous les paramètres mesurés. Ce projet de bachelor correspond à la mise en place et l’optimisation de cette plateforme, qui doit pouvoir représenter en priorité la répartition du trafic sur toute la ville et son évolution quotidienne. Le bruit et la qualité de l’air en relation avec le trafic routier seront aussi illustrés.
Les mesures stockées sur le repository BBData de l'HEIA-FR devront être transférées vers l'application de visualisation. L'entreprise softcom active dans le domaine des smart cities accompagnera le projet pour assurer un développement industrialisable. En tant que cliente, les villes de Fribourg et Bulle définiront le type de visualisation désirée et le format des fichiers à extraire.


## Objectifs

Les objectifs et étapes du projet sont décrits ci-dessous de manière chronologique :

1)  Faire l'état de l'art des solutions utilisées pour le stockage et la visualisation de données smart city

2)  Faire une analyse des besoins en terme de visualisation, accès aux données, découplage des services (e.g. avec API REST)

4)  Sélectionner et définir la meilleure façon de représenter un flux de trafic bidirectionnel sur les axes de la ville avec une estimation du trafic se trouvant entre les points de mesures

5)  Développer en fonction de la sélection quelques modules de visualisation des données en visant des fonctionnalités avancées: rapport hebdomadaires, données sur cartes, navigation temporelle sur time series, etc.

6)  Définir et réaliser le système de visualisation de la pollution et du bruit avec tous les capteurs disponibles

7)  Valider la solution




## Contraintes

Utiliser les outils déjà développés par le projet FRI-IoTnet, se coordonner avec ce projet, s'adapter aux besoins de la ville, extraire les données du repository BBData

---
version: 2
titre: DEMO-MI2 reloaded
type de projet: Projet de bachelor
année scolaire: 2020/2021
filières:
  - Informatique
  - Télécommunications
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Nicolas Schroeter
mandants: [ville de Fribourg]
mots-clé: [IoT, Data Science, statistical analysis, multiple parameter analysis, smart city, life test, interactive web app]
langue: [F, D]
confidentialité: non
suite: non
---
```{=tex}
\begin{center}
\includegraphics[width=0.7\textwidth]{img/pavillon.jpg}
\end{center}
```

## Contexte

La ville de Fribourg et l’HEIA-FR ont lancé le projet d'un pavillon de démonstration des moyens naturels de rafraîchissement d’un espace public extérieur pour les périodes de canicule. Un travail de semestre PS6 a permis de développer le système embarqué pour mesurer: température, humidité, vent, rayonnement solaire incident et qualité de l’air de même que l’infrastructure de communication, le système de stockage des données et les applications de visualisation. Le pavillon mobile innovant https://wemakeit.com/projects/demo-mi2-2020?locale=fr sera monté au mois de juin entre l’Université et l’HEIA-FR et sera déplacé durant l’été sur différentes places publiques. Si le travail de semestre s’est focalisé sur le développement de l’infrastructure « TIC », le projet de bachelor sera dédié à l'analyse détaillée et innovante des mesures et ceci dans un contexte d'évolution climatique.

Les défauts de jeunesse du système développé au PS6 seront corrigés avec un effort particulier sur la visualisation attractive. Bernard Sturny du Service de l'environnement (Sen) a annoncé qu'il était prêt à collaborer au projet en transmettant les mesures "live" des stations automatiques du parc Domino et de la route du Jura et en donnant l'accès aux mesures historiques. De cette façon l'aspect comparatif devra être exploité avec le but d'illustrer les différences entre quartiers et de visualiser l'évolution des données climatiques comme point de mire les mesures du pavillon demo-mi2.
Les correlations entre le trafic routier, mesuré par le projet « FRI-IoT net » et la qualité de l’air pourront aussi être étudiées avec une communication des résultats via l'API du projet demo-mi2. Les effets de la météo et de la géographie devront être pris en compte et les méthodes d’évaluation des dépendances (convolutions ou autre solutions) seront choisies par l’étudiant. Dans la mesure du possible, des prévisions d’évolution des paramètres pour les prochaines heures seront calculées et affichées.
L’effet de rafraîchissement du Pavillon durant la canicule pourra aussi être analysé en fonction des heures de la journée. Un résumé de ces comparaisons, évolutions et interdépendance devra être rendu accessible aux visiteurs par un « upgrade » de l’application développée durant le travail de semestre PS6.

## Objectifs

Les objectifs et étapes du projet sont décrits ci-dessous de manière chronologique :

1)	Faire un récapitulatif sur tout ce qui a été développé durant le travail de semestre PS6 sur le pavillon DEMO-MI2 et son infrastructure de mesure, en décrivant ce qui doit encore être finalisé avant de passer à l’analyse des données.
2)	Faire un état de l’art des systèmes de test de corrélation entre différentes variables. Évaluer les différentes solutions et sélectionner le système le plus prometteur pour calculer et illustrer la dépendance de la qualité de l’air (densité de microparticules) des autres paramètres.

3)	Développer un système de consultation des mesures du service de l’environnement et de visualisation géographique et évolutive des paramètres mesurés.

4)	Utiliser la méthode sélectionnée sous 2) pour établir et chiffrer les corrélations et contrôler leur véracité avec le service de l’environnement

5)	Développer un système prévisionnel et l’intégrer dans l’application web interactive développée durant le travail de semestre.


## Contraintes

s'adapter aux besoins et au calendrier du projet demo-mi2, la livraison de certains équipements dès le départ est par contre un avantage.


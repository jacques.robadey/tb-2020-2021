---
version: 2
titre: localiser les véhicules de la ville de Fribourg
type de projet: Projet de bachelor
année scolaire: 2020/2021
filières:
  - Informatique
  - Télécommunications
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Jean-Roland Schuler
mandants: [ville de Fribourg]
proposé par étudiant: Dylan Marmy
mots-clé: [LoRa, Internet des objets, positionnement gps, services de la ville, véhicules communaux]
langue: [F, D]
confidentialité: non
suite: non
---


## Contexte

La ville de Fribourg désire avoir un suivi de ses véhicules de transport de déchets avec l'enregistrement des coordonnées gps de leurs arrêts pour optimiser leurs trajets. Grâce au réseau LoRa et aux récents travaux de semestre de la filière ISC, il est possible de connaitre la position de véhicules en y positionnant des gps munis d'émetteurs Lora. L’avantage est de ne pas recourir au gsm et à la 5G et d’être ainsi indépendant d’un opérateur télécom. Les données pourraient cependant ne pas être transmises dû à une mauvaise couverture ou un débit trop lent. Pour ceci il faut stocker les données et les envoyer aux temps opportun. Un émetteur WLAN ou une carte SIM doit être intégré pour pouvoir uploader des données mesurées lorsque les véhicules ont terminé leur trajet.
La ville de Fribourg a manifesté son intérêt pour les camions-poubelles. Un intérêt semblable existe pour les chasse-neige afin de découvrir le temps nécessaire pour les différents tronçons et pour toute entreprise disposant d'un grand parc de véhicule.


## Objectifs

Les objectifs et étapes du projet sont décrits ci-dessous de manière chronologique :

1)	Faire un état de l’art des différentes solutions de détection de l’arrêt d’un véhicule et de l’envoi de sa position à ce moment précis et un résumé des travaux déjà entrepris à l'HEIA-FR

2)	Développer et optimiser un système embarqué permettant de détecter l’arrêt, le mouvement d’un objet, et l’envoi de sa position et de l’heure par LoRa à un serveur centralisé

3)	Définir une stratégie pour rassembler et mémoriser localement les informations pertinentes avant de les envoyer par bloc vers le serveur

4)	trouver une solution pour palier au manque de connectivité dans des endroits critiques de la ville et envoyer les données mesurées dès que la couverture du réseau est à nouveau fonctionnelle et/ou lorsque les trajets sont terminés

5)	Développer une application permettant de voir les trajets des différents véhicules et identifiant l’endroit de leurs arrêts qui pourrait être utilisée pour toute ville ou entreprise désirant connaître l'emplacement et le déplacement de ses véhicules.



## Contraintes

Utilisation de traqueurs gps avec interface LoRa, utiliser le réseau FRI-LoRanet, s'adapter aux besoins de la ville
